const nav = [
  { text: '组件', link: '/' },
  { text: '版本历程', link: 'https://gitee.com/devui/vue-devui/releases' },
  { text: '设计规范', link: 'https://devui.design/design-cn/start' },
]

export default nav